package com.example.myapplication


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val API_KEY = "8c19102bf0c2d10276ee905ca3c6791c"

interface WeatherService {
        @GET("/data/2.5/weather?")
    fun getWeatherByName(
        @Query("appid")
        apiKey: String = API_KEY,
        @Query("q")
        name: String = "Paris",
        @Query("units")
        units: String = "metric"
    ): Call<Weather>
}