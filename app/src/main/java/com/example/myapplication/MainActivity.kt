package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val textView = findViewById<View>(R.id.hello) as TextView
        Log.d("MainActivity", "Coucou je suis dans le OnCreate()")


        try {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val weatherService: WeatherService = retrofit.create(WeatherService::class.java)
            val call = weatherService.getWeatherByName(name = "Paris")

            call.enqueue(object : Callback<Weather> {
                override fun onResponse(call: Call<Weather>, response: Response<Weather>) {
                    textView.text = response.body()?.weather?.first()?.description
                    Log.i("MainActivityTest", "Test api" + response.body().toString())
                    Log.i("Test", call.request().url().toString())
                }

                override fun onFailure(call: Call<Weather>, throwable: Throwable) {
                    Log.e("MainActivityError", throwable.message.toString())
                }


            })

        } catch (e: Exception) {
            Log.d("MainActivity", e.message.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("MainActivity", "je suis dans le start")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MainActivity", "je suis dans la pause")
    }
}